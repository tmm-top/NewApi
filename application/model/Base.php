<?php
/**
 *
 * @since   2019-04-23
 * @author  zhaoxiang <zhaoxiang051405@gmail.com>
 */

namespace app\model;


use think\Model;

class Base extends Model {
    protected $autoWriteTimestamp = true;
    //定义自动完成的时间戳的实际字段
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
}
