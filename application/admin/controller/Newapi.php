<?php

namespace app\admin\controller;

use app\model\DbNewapi;
use app\util\ReturnCode;
use app\util\Tools;

class Newapi extends Base
{
    public function index()
    {
        $limit = $this->request->get('size', config('apiadmin.ADMIN_LIST_DEFAULT'));
        $start = $this->request->get('page', 1);
        $keywords = $this->request->get('keywords', '');
        $obj = new DbNewapi();
        // $obj = $obj->whereLike('', '%%');
        $listObj = $obj->order('id')->paginate($limit, false, ['page' => $start])->toArray();
        return $this->buildSuccess([
            'list'  => $listObj['data'],
            'count' => $listObj['total']
        ]);
    }
    public function add()
    {
        $postData = $this->request->post();
        $res = DbNewapi::create($postData);
        if ($res === false) {
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR, '操作失败！');
        } else {
            return $this->buildSuccess([]);
        }
    }
    public function edit()
    {
        $postData = $this->request->post();
        $res = DbNewapi::update($postData);
        if ($res === false) {
            return $this->buildFailed(ReturnCode::DB_SAVE_ERROR, '操作失败！');
        } else {
            return $this->buildSuccess([]);
        }
    }
    public function del()
    {
        $id = $this->request->get('id');
        if (!$id) {
            return $this->buildFailed(ReturnCode::EMPTY_PARAMS, '缺少必要参数！');
        } else {
            DbNewapi::destroy($id);
            return $this->buildSuccess([]);
        }
    }
}
