CREATE TABLE `db_newapi` (
	`id` INT ( 11 ) NOT NULL AUTO_INCREMENT COMMENT '编号',
	`appname` VARCHAR ( 50 ) NULL COMMENT '项目名称',
	`sort` INT ( 11 ) NULL DEFAULT 0 COMMENT '排序',
	`update_time` INT ( 11 ) NULL DEFAULT 0 COMMENT '更新时间',
	`create_time` INT ( 11 ) NULL DEFAULT 0 COMMENT '创建时间',
	`status` INT ( 11 ) NULL DEFAULT 0 COMMENT '状态',
	PRIMARY KEY ( `id` )) 
    COMMENT='NewApi';