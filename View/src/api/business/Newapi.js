
            import axios from '@/libs/api.request'
            export const getList = (params) => {
              return axios.request({
                url: 'Newapi/index',
                method: 'get',
                params: params
              })
            }
            export const add = (data) => {
              return axios.request({
                url: 'Newapi/add',
                method: 'post',
                data
              })
            }
            export const edit = (data) => {
              return axios.request({
                url: 'Newapi/edit',
                method: 'post',
                data
              })
            }
            export const del = (id) => {
              return axios.request({
                url: 'Newapi/del',
                method: 'get',
                params: {
                  id: id
                }
              })
            }